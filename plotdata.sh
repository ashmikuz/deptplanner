#!/bin/bash

TESTNUMBER=2
LECTURENO=$(cat "tempistiche-0" | wc -l )
#LECTURENO=11

for((J=0;J<LECTURENO; J++));do 
    AVG[$J]=0
    MAX[$J]=0
    MIN[$J]=99999
    SUMSQUARES[$J]=0
done

for((I=0;I<TESTNUMBER;I++)); do
    for((J=0; J<LECTURENO; J++));do
        NLINE=$((J + 1))
        VAL=$(sed $NLINE'q;d' "tempistiche-"$I)
        AVG[$J]=$(echo "${AVG[$J]} + $VAL" | bc -l)
        SUMSQUARES[$J]=$(echo "${SUMSQUARES[$J]} + $VAL^2" | bc -l) 
        #echo "${SUMSQUARES[$J]} + $VAL^2" 
        TMP=$VAL
        if [[ $(echo "$VAL < ${MIN[$J]}" | bc -l) -eq 1 ]] ; then
          MIN[$J]=$VAL 
        fi

        if [[ $(echo "$VAL > ${MAX[$J]}"| bc -l) -eq 1 ]]; then
            MAX[$J]=$VAL
        fi
    done
done

for((J=0; J<LECTURENO; J++)); do
    AVG[$J]=$(echo " ${AVG[$J]} / $TESTNUMBER " | bc -l)
    STDDEV[$J]=$(echo "(sqrt(1/($TESTNUMBER-1) * (${SUMSQUARES[$J]}-$TESTNUMBER*(${AVG[$J]}^2))))" | bc -l)
    MAX[$J]=$(echo "${AVG[$J]} + ${STDDEV[$J]}" | bc -l)
    MIN[$J]=$(echo "${AVG[$J]} - ${STDDEV[$J]}" | bc -l)
    echo ${STDDEV[$J]}
done

touch plotdata.csv
rm plotdata.csv
for((J=0; J<LECTURENO; J++)); do
    echo "${AVG[$J]}, ${MIN[$J]}, ${MAX[$J]}" >> plotdata.csv
done

PLOT_STRING="set xlabel \"Lezioni Rimosse\";
set ylabel \"Tempo\"; set datafile separator \",\";
set term png size 800,600;
set output \"tempimedi.png\";
plot \"plotdata.csv\" using (column(0)):2 with lp lt 1 pt 7 ps 1.5 lw 3 t \"Tempi medi\""

echo $PLOT_STRING

echo $PLOT_STRING | gnuplot;

