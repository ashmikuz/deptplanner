#!/usr/bin/env python3

#    This file is part of deptplanner.
#
#    Copyright (c) 2015 Daniele Baschieri, Michele Corazza.
#
#    deptplanner is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    deptplanner is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with deptplanner.  If not, see <http://www.gnu.org/licenses/>.

import os
import re
import math
import copy
import sys,getopt,subprocess
import random

DATA_FOLDER="./data/"
DATA_FOLDER_ORIGINAL="data-fistterm"
LECTURES_FOLDER=DATA_FOLDER+"lectures/"
TEACHERS_FOLDER=DATA_FOLDER+"teachers/"
ROOMS_FOLDER=DATA_FOLDER+"rooms/"
GLOBAL_FILE=DATA_FOLDER+"global/global"

curricula='Informatica per il Management II Anno'
vuoto=  'Curriculum: []'
def removeCurricula(curricula):
    for lecturefile in os.listdir(LECTURES_FOLDER):
        lfile=open(LECTURES_FOLDER+lecturefile,"r+")
        text=lfile.read();
        #print("==>apro "+lecturefile)
        #print(text);
        if(text.find(', "'+curricula+'"')!=-1):
            print('updated: '+lecturefile)
            text=text.replace(', "'+curricula+'"','')
            lfile.seek(0)
            lfile.write(text)
            lfile.truncate()
        if(text.find('"'+curricula+'",')!=-1):
            print('updated: '+lecturefile)
            text=text.replace('"'+curricula+'",','')
            lfile.seek(0)
            lfile.write(text)
            lfile.truncate()
        if(text.find('"'+curricula+'"')!=-1):
            #print('======>rimuovo '+lecturefile)
            print('deleted: '+lecturefile)
            os.remove(LECTURES_FOLDER+lecturefile)            
        lfile.close()

def main():
    curricula=[]    
    curricula.append("Informatica Magistrale I Anno")
    curricula.append("Informatica Magistrale II Anno, Management")
    curricula.append("Informatica Magistrale II Anno, Sistemi e Reti")
    curricula.append("Informatica Magistrale II Anno, Progettazione e Analisi")
    curricula.append("Informatica per il Management I Anno")
    curricula.append("Informatica per il Management II Anno")    
    curricula.append("Informatica per il Management III Anno")
    curricula.append("Informatica III Anno")
    curricula.append("Informatica II Anno")
    curricula.append("Informatica I Anno")
    for j in range(0,10):
        print(j)
        subprocess.call(["touch","data"])
        subprocess.call(["rm", "-r", "data"])
        subprocess.call(["cp", "-r", DATA_FOLDER_ORIGINAL, "data"])
        random.shuffle(curricula)
        for q in curricula:              
            subprocess.call(["./remCur.sh", str(j)])
            removeCurricula(q)  
            print(q) 
    

main()
