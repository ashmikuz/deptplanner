#!/bin/bash

#    This file is part of deptplanner.
#
#    Copyright (c) 2015 Daniele Baschieri, Michele Corazza.
#
#    deptplanner is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    deptplanner is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with deptplanner.  If not, see <http://www.gnu.org/licenses/>.


export PWD=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

./generatedata.py > data.dzn;
if [[ ! -s data.dzn ]] ; then 
    echo "could not populate data file, exiting...";
    exit -1
fi
mzn-g12lazy deptplanner.mzn data.dzn | head -n1 >  output;
if [[ ! -s output ]] ; then 
    echo "could not compute output, exiting...";
    exit -1
fi
./generateoutputtable.py -i output -e data.dzn -o orario.html -f html;
#xdg-open orario.html &> /dev/null
