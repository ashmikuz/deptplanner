#!/usr/bin/env python3

#    This file is part of deptplanner.
#
#    Copyright (c) 2015 Daniele Baschieri, Michele Corazza.
#
#    deptplanner is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    deptplanner is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with deptplanner.  If not, see <http://www.gnu.org/licenses/>.

import os
import re
import math
import copy

DATA_FOLDER="./data/"
LECTURES_FOLDER=DATA_FOLDER+"lectures/"
TEACHERS_FOLDER=DATA_FOLDER+"teachers/"
ROOMS_FOLDER=DATA_FOLDER+"rooms/"
GLOBAL_FILE=DATA_FOLDER+"global/global"

numberlectures=0
numberTeachers=0
numberRooms=0
numberCurriculas=0

maxlectureHours=9
maxlectureHoursInDay=4
maxTeacherHours=6
maxCurriculumHours=8
minlectureHours=2
minDaylectures=1

lectures=[]
teachers=[]
rooms=[]
teacheridx={}
lectureidx={}
roomidx={}
curriculumidx={}

nc=1
nt=1
nr=1
nl=1

class TimeInterval:
    def checkconsistency(self):
        if(self.endday<self.startday):
            print("End day is smaller than start day")
            exit(-1)
        if(self.endhour<self.starthour):
            print("End hour is smaller than start hour")
            exit(-1)
        if(self.starthour not in range(8,18)):
            print("start hour not in range")
            exit(-1)
        if(self.endhour not in range(8,18)):
            print("end hour not in range")
            exit(-1)
    
    def __init__(self, string):
        res=re.match(r"([1-5])\-?([1-5])?\|?([0-9]+)?-?([0-9]+)?",string)
        self.startday=int(res.group(1))
        if(res.group(2) is not None):
            self.endday=int(res.group(2))
        else:
            self.endday=startday
        if(res.group(3) is not None):
            self.starthour=int(res.group(3))
        else:
            self.starthour=8
        if(res.group(4) is not None):
            self.endhour=int(res.group(4))
        else:
            self.endhour=17
        self.checkconsistency()
      


class Lecture:
    def __init__(self,filename):
        global nc
        lecturefile=open(LECTURES_FOLDER+filename)
        for line in lecturefile.readlines():
            res=re.match(r"(.+): *(.*)", line)
            parName=res.group(1)
            parValue=res.group(2)
            if(parName=="Curriculum"):
                parValue=eval(parValue)
                for curriculumVal in parValue:
                    if(curriculumVal not in curriculumidx):
                        curriculumidx[curriculumVal]=nc
                        nc=nc+1                        
            elif("[" in res.group(2)):
                parValue=eval(parValue)
            setattr(self,parName, parValue)
            
class Teacher:
    def __init__(self,filename):
        teacherfile=open(TEACHERS_FOLDER+filename)
        for line in teacherfile.readlines():
            res=re.match(r"(.+): *(.+)", line)
            parName=res.group(1)
            parValue=res.group(2)
            if("[" in res.group(2)):
                parValue=eval(parValue)
                self.timeList=[]
                for val in parValue:
                    self.timeList.append(TimeInterval(val))
                    parValue=timeList
            setattr(self,parName, parValue)
            
    def isUnavaliable(self,d,h):
        for time in self.UnavaliableSlots:
            if(d in range(time.startday,time.endday+1) and (h in range(time.starthour,time.endhour+1))):
                return True;
        return False;
        
class Room:
    OnlyRequested=False
    def __init__(self,filename):
        roomfile=open(ROOMS_FOLDER+filename)
        for line in roomfile.readlines():
            res=re.match(r"(.+): *(.*)", line)
            parName=res.group(1)
            parValue=res.group(2)
            if("[" in res.group(2)):
                parValue=eval(parValue)
                self.timeList=[]
                for val in parValue:
                    self.timeList.append(TimeInterval(val))
                    parValue=self.timeList
            setattr(self,parName, parValue)
    def isUnavaliable(self,d,h):
        for time in self.UnavaliableSlots:
            if(d in range(time.startday,time.endday+1) and (h in range(time.starthour,time.endhour+1))):
                return True;
        return False;

def boolToInt(value):
    if(value=="True"):
        return "1"
    else:
        return "0"
def getLecturePosition(value):
    if(value=='""'):
        return "0"
    else:
        for l in lectures:
            if(l.Name==value):
                return lectures.index(l)+1
        return "0"

def getLecture(value):
    if(value=='""'or not (value in lectureidx)):
        return "0"
    else:
        return lectureidx[value]

def getRoom(value):
    if(value=='""'):
        return "0"
    else:
        return roomidx[value]

def printGlobals():
    globalfile=open(GLOBAL_FILE)
    for line in globalfile.readlines():
        print(line, end="")
        
def output_data():
    
    print("nt="+str(nt-1)+";")
    print("nr="+str(nr-1)+";")
    print("nc="+str(nc-1)+";")
    print("nl="+str(len(lectures))+";")
    printGlobals()
    maxlecturehours=0
    maxlectureindex=0;
    teacherstr="lectureteacher=["
    curriculumstr="lecturecurriculum=["
    studentnostr="lecturestudentno=["
    lecturehourstr="lecturehours=["
    optionallecturesstr="optionallecture=["
    lectureindexstr="lectureindex=["
    successivelecturestr="successivelecture=["
    conflictinglecturestr="conflictinglecture=["
    splitstudentsstr="splitstudents=["
    requestedroomstr="requestedroom=["
    lecturenames="%lecturenames=["
    curriculumnames="%curriculumnames=["
    for l in lectures:
        lteacher=teacheridx[l.Teachers[0]]
        lcurriculum=curriculumidx[l.Curriculum[0]]
        teacherstr=teacherstr+str(lteacher)
        curriculumstr=curriculumstr+str(lcurriculum)
        studentnostr=studentnostr+l.StudentNo
        lecturehourstr=lecturehourstr+str(l.TeacherNumHours[0])
        optionallecturesstr=optionallecturesstr+boolToInt(l.Optional)
        successivelecturestr=successivelecturestr+str(getLecturePosition(l.SuccessiveLecture))
        lectureindexstr=lectureindexstr+str(lectureidx[l.Name])
        conflictinglecturestr=conflictinglecturestr+str(getLecture(l.Conflict))
        splitstudentsstr=splitstudentsstr+boolToInt(l.SplitStudents)
        requestedroomstr=requestedroomstr+str(getRoom(l.RequestedRoom))
        lecturenames=lecturenames+'"'+(l.Name)+'"'
        if(l!=lectures[-1]):
            teacherstr=teacherstr+","
            curriculumstr=curriculumstr+","
            studentnostr=studentnostr+","
            lecturehourstr=lecturehourstr+","
            optionallecturesstr=optionallecturesstr+","
            successivelecturestr=successivelecturestr+","
            lectureindexstr=lectureindexstr+","
            conflictinglecturestr=conflictinglecturestr+","
            splitstudentsstr=splitstudentsstr+","
            requestedroomstr=requestedroomstr+","
            lecturenames=lecturenames+","
        if(maxlecturehours<int(l.TeacherNumHours[0])):
            maxlecturehours=int(l.TeacherNumHours[0])
        if(maxlectureindex<int(lectureidx[l.Name])):
            maxlectureindex=int(lectureidx[l.Name])

    teacherstr=teacherstr+"];"
    curriculumstr=curriculumstr+"];"
    studentnostr=studentnostr+"];"
    lecturehourstr=lecturehourstr+"];"
    optionallecturesstr=optionallecturesstr+"];"
    successivelecturestr=successivelecturestr+"];"
    lectureindexstr=lectureindexstr+"];"
    conflictinglecturestr=conflictinglecturestr+"];"
    splitstudentsstr=splitstudentsstr+"];"
    requestedroomstr=requestedroomstr+"];"
    lecturenames=lecturenames+"];"
    
    print("maxlectureindex="+str(maxlectureindex)+";")
    print(teacherstr)
    print(curriculumstr)
    print(studentnostr)
    print(lecturehourstr)
    print(optionallecturesstr)
    print(lectureindexstr)
    print(successivelecturestr)
    print(conflictinglecturestr)
    print(splitstudentsstr)
    print(requestedroomstr)
    print(lecturenames)
    
    roomcapacitystr="roomcapacity=["
    roomnames="%roomnames=["
    onlyrequested="onlyrequested=["
    for r in rooms:
        roomcapacitystr=roomcapacitystr+r.Capacity
        roomnames=roomnames+'"'+r.Name+'"'
        onlyrequested=onlyrequested+boolToInt(r.OnlyRequested)
        if(r!=rooms[-1]):
            roomcapacitystr=roomcapacitystr+","
            roomnames=roomnames+","
            onlyrequested=onlyrequested+","
    roomcapacitystr=roomcapacitystr+"];"
    roomnames=roomnames+"];"
    onlyrequested=onlyrequested+"];"
    print(roomcapacitystr)
    print(roomnames)
    print(onlyrequested)
    consecutivedaysstr="requestedconsecutivedays=["
    teachernames="%teachernames=["
    for t in teachers:
        consecutivedaysstr=consecutivedaysstr+str(t.RequestedConsecutiveDays)
        teachernames=teachernames+'"'+t.Name+'"'
        if(t!=teachers[-1]):
            consecutivedaysstr=consecutivedaysstr+","
            teachernames=teachernames+","
    consecutivedaysstr=consecutivedaysstr+"];"
    teachernames=teachernames+"];"
    print(consecutivedaysstr)
    print(teachernames)
    
    
    maxinstances=int(math.floor(maxlecturehours/2));
    allocationrequestesstr="allocationrequests=[|"
    for l in lectures: 
            allocationrequestesstr=allocationrequestesstr+""
            val=0
            for val in range(0,len(l.AllocReq[0])):
                allocationrequestesstr=allocationrequestesstr+str(l.AllocReq[0][val])+","
            
            for v2 in range(len(l.AllocReq[0]),maxinstances):
                allocationrequestesstr=allocationrequestesstr+"0,"
            allocationrequestesstr=allocationrequestesstr[:-1]
            allocationrequestesstr=allocationrequestesstr+"|"
    allocationrequestesstr=allocationrequestesstr+"];"
                
    print(allocationrequestesstr)   
    print("maxlecturehours="+str(maxlecturehours)+";")     
    print("maxinstances="+str(maxinstances)+";")
    
    teacheravaliabilitystr="teacheravaliability = array3d(1.."+str(nt-1)+",1..5,8..17, [";
    for h in range(1,11):
        for d in range(1,6):
            for t in teachers:
                if(t.isUnavaliable(d,h)):
                    teacheravaliabilitystr=teacheravaliabilitystr+'0,'
                else:
                    teacheravaliabilitystr=teacheravaliabilitystr+'1,'
    teacheravaliabilitystr=teacheravaliabilitystr[:-1]
    teacheravaliabilitystr=teacheravaliabilitystr+']);'
    
    print(teacheravaliabilitystr)
    
    roomavaliabilitystr="roomavaliability = array3d(1.."+str(nr-1)+",1..5,8..17, [";
    for h in range(1,11):
        for d in range(1,6):
            for r in rooms:
                if(r.isUnavaliable(d,h)):
                    roomavaliabilitystr=roomavaliabilitystr+'0,'
                else:
                    roomavaliabilitystr=roomavaliabilitystr+'1,'
    roomavaliabilitystr=roomavaliabilitystr[:-1]
    roomavaliabilitystr=roomavaliabilitystr+']);'
    
    print(roomavaliabilitystr)

    
    curriculumarray=[]
    
    for cidx in range(0, nc-1):
        curriculumarray.append("")
    
    for c in curriculumidx:
        curriculumarray[curriculumidx[c]-1]=c
    
    curriculumnames="%curriculumnames=["
    
    for c in curriculumarray:
        curriculumnames=curriculumnames+'"'+c+'"'
        if(c!=curriculumarray[-1]):
            curriculumnames=curriculumnames+","
    
    curriculumnames=curriculumnames+"];"
    
    print(curriculumnames)

def splitLecture(l):
    temp=[]
    for tIndex in range(0,len(l.Teachers)):
        for cIndex in range(0,len(l.Curriculum)):
            ltemp=copy.deepcopy(l)
            ltemp.Teachers=[l.Teachers[tIndex]]
            #print(ltemp.Name+" : "+l.Teachers[tIndex]+" "+str(tIndex)+" ");
            ltemp.Curriculum=[l.Curriculum[cIndex]]
            ltemp.TeacherNumHours=[l.TeacherNumHours[tIndex]]
            allocindex=min(tIndex, len(l.AllocReq)-1)
            ltemp.AllocReq=[l.AllocReq[allocindex]]
            temp.append(ltemp)
    return temp
        
def parse_lectures():
    global nl
    global nt
    global nr
    global lectures
    for lecturefile in os.listdir(LECTURES_FOLDER):
        split_lecture_list=splitLecture(Lecture(lecturefile))
        temp_list=[]
        isSplitted=False
        for s in split_lecture_list:
            if(s.SplitStudents=='True'):
                l1temp=copy.deepcopy(s)
                l2temp=copy.deepcopy(s)
                l1temp.Name=l1temp.Name+" A-M";
                l2temp.Name=l2temp.Name+" M-Z";
                l1temp.TeacherNumHours=[l1temp.TeacherNumHours[0] // 2]
                l2temp.TeacherNumHours=[l2temp.TeacherNumHours[0] // 2]
                #l1temp.Conflict=l2temp.Name
                #l2temp.Conflict=l1temp.Name
                l1temp.SuccessiveLecture=l2temp.Name
                temp_list.append(l1temp)
                temp_list.append(l2temp)
                isSplitted=True
            else:
                temp_list.append(s)
        split_lecture_list=temp_list        
        if(isSplitted==True):
            lectures=lectures+split_lecture_list
            lectureidx[split_lecture_list[0].Name]=nl
            nl=nl+1
            lectureidx[split_lecture_list[1].Name]=nl
            nl=nl+1
        else:
            lectures=lectures+split_lecture_list
            lectureidx[split_lecture_list[0].Name]=nl
            nl=nl+1
    for teacherfile in os.listdir(TEACHERS_FOLDER):
        curr_teacher=Teacher(teacherfile)
        for lect in lectures:
            if(curr_teacher.Name in lect.Teachers):
                teachers.append(curr_teacher)
                teacheridx[curr_teacher.Name]=nt
                nt=nt+1
                break
    for roomfile in os.listdir(ROOMS_FOLDER):
        curr_room=Room(roomfile)
        if(curr_room.OnlyRequested):
            for l in lectures:
                if(curr_room.Name == l.RequestedRoom):
                    rooms.append(curr_room)
                    roomidx[curr_room.Name]=nr
                    nr=nr+1
                    break
        else:
            rooms.append(curr_room)
            roomidx[curr_room.Name]=nr
            nr=nr+1
    output_data()

os.chdir(os.path.dirname(os.path.realpath(__file__)))
parse_lectures()
