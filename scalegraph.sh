#!/bin/bash

#    This file is part of deptplanner.
#
#    Copyright (c) 2015 Daniele Baschieri, Michele Corazza.
#
#    deptplanner is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#   
#    deptplanner is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with deptplanner.  If not, see <http://www.gnu.org/licenses/>.

export TIMEFORMAT="%R"
DATA_FILE="data-fistterm"
NUMTESTS=10
IFS=$'\n'
for((j=0 ; j<NUMTESTS; j++)); do
    echo $j
    touch data
    rm -r data
    cp -r $DATA_FILE data
    for i in `find data/lectures -type f | sort -R`; do
        (time ./executeplanner.sh) &>> tempistiche-$j
        rm "$i";
        echo $i     
    done 
done

./plotdata.sh
