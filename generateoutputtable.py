#!/usr/bin/env python2
# -*- coding: utf-8 -*-

#    This file is part of deptplanner.
#
#    Copyright (c) 2015 Daniele Baschieri, Michele Corazza.
#
#    deptplanner is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    deptplanner is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with deptplanner.  If not, see <http://www.gnu.org/licenses/>.

import sys, getopt

def load_orario(file_orario):
        in_file = open(file_orario,"r")
        dati=in_file.read()
        in_file.close()
        return dati
        
def load_curriculum(file_filter):
        in_file = open(file_filter,"r")
        for line in in_file.readlines():
            values=line.split("=")
            if(values[0]=="lecturecurriculum"):
                filter_curriculum=values[1][1:-3].split(",")
                in_file.close()
                return filter_curriculum

def getArray(file_filter, param):
    in_file = open(file_filter,"r")
    for line in in_file.readlines():
        values=line.split("=")
        if(values[0]==param):
            return(eval(values[1][:-2]))
        

def getParameter(file_filter,param):
    in_file = open(file_filter,"r")
    res=-1
    for line in in_file.readlines():
        values=line.split("=")
        if(values[0]==param):
            res=int(values[1][0:-2])
    in_file.close()
    return res
                
def getRooms(dati):
    bounds=dati[dati.index("("):dati.index("[")]
    bounds=bounds.split(",")
    rooms=bounds[1].split("..")[1]
    return int(rooms)

def getTable(dati,rooms,lectures):
        occupati=dati[dati.index("[")+1:dati.index("]")]
        occupati=occupati.split(",")
        result=[]
        for day in range(0,5):
                result.append([]);
                for hour in range(0,10):
                        result[day].append([]);
        for day in range(0,5):
                for hour in range(0,10):
                        for room in range(0,rooms):
                                for lecture in range(0,lectures):
                                    idx=hour+day*10+room*5*10+lecture*rooms*5*10
                                    if( "1" in occupati[idx] ):
                                        result[day][hour].append([lecture,room])
        return result

def stampa(tabella):
        for hour in range(0,10):
                if(hour+8<=9):
                        print (""),
                print (""+str(hour+8)+": "),
                for day in range(0,5):
                        print (str(tabella[day][hour])),
                        if(not (len(tabella[day][hour])>0)):
                                print ("          "),
                print ""

def stampaFilter(tabella,filter_curriculum,curriculum):
        x=''
        for hour in range(0,10):
                if(hour+8<=9):
                        x+=" "
                x+= (""+str(hour+8)+": ")
                for day in range(0,5):
                        find = 0;
                        for elem in tabella[day][hour]:
                                if str(filter_curriculum[elem[0]])==str(curriculum):
                                        if(elem[0]>9):
                                                x+= (str(elem))
                                        else:
                                                x+= (" "+str(elem))	
                                        find = 1
                                        
                        if(not (len(tabella[day][hour])>0) or not (find==1)):
                                x+= "[     ]"
                x+= "\n"
        return x

def stampaFilterRoom(tabella,room):
        x=''
        for hour in range(0,10):
                if(hour+8<=9):
                        x+= (" ")
                x+=  (""+str(hour+8)+": ")
                for day in range(0,5):
                        find = 0;
                        for elem in tabella[day][hour]:
                                if str(elem[1])==str(room):
                                        if(elem[0]>9):
                                                x+=  (str(elem))
                                        else:
                                                x+=  (" "+str(elem))
                                        find = 1
                                        
                        if(not (len(tabella[day][hour])>0) or not (find==1)):
                                x+=  ("[     ]")
                x+=  "\n"
        return x
def grechina(name):
        x=''
        x+= "\n"
        x+= "=================== "+str(name)+" ===================\n"
        x+= "\n"
        return x
def getTableHeaderHTML():
    x='<tr>\n<td></td>\n';
    giorni=['lunedì','martedì','mercoledì','giovedì','vernerdì'];
    for day in range(0,5):
        x=x+'<td>'+giorni[day]+'</td>\n'
    x=x+'<tr/>\n'
    return x
def generateTable(inputfile,file_ambiente):		
        orario=load_orario(inputfile);
        filter_curriculum=load_curriculum(file_ambiente)
        rooms=getParameter(file_ambiente,"nr")
        lectures=getParameter(file_ambiente,"nl")
        curricula=getParameter(file_ambiente,"nc")		
        table=getTable(orario,rooms,lectures);
        x=''
        x+=grechina("CORSI")
        for curriculum in range(1,curricula+1):
                x+= "Tabella corso: "+str(curriculum)+"\n"
                x+=stampaFilter(table,filter_curriculum,curriculum)
                x+= "\n"
        x+=grechina("AULE")
        for room in range(0,rooms):
                x+= "Tabella Aule: "+str(room)+"\n"
                x+=stampaFilterRoom(table,room)
                x+= "\n"
        return x


def generateTableHTML(inputfile,file_ambiente):        
        orario=load_orario(inputfile);
        filter_curriculum=load_curriculum(file_ambiente)
        rooms=getParameter(file_ambiente,"nr")
        lectures=getParameter(file_ambiente,"nl")
        curricula=getParameter(file_ambiente,"nc")	
        curriculumNames=getArray(file_ambiente,"%curriculumnames")
        lectureNames=getArray(file_ambiente,"%lecturenames")
        roomNames=getArray(file_ambiente, "%roomnames")
        table=getTable(orario,rooms,lectures);
        x='<link rel="stylesheet" type="text/css" href="default.css" />\n'
        x=x+'<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />'
        x+="<div class=\"left\"><h1>CURRICULA</h1>\n"
        for curriculum in range(1,curricula+1):
			x+= "<h2>Tabella curriculum: "+curriculumNames[curriculum-1]+"</h2>\n"
			x+=generaKindHTML(table,0,curriculum,filter_curriculum,lectureNames,roomNames)
			x+= "\n"
        x+='</div>'        
        x+="<div class=\"left\"><h1>AULE</h1>\n"
        for room in range(0,rooms):
                x+= "<h2>Tabella Aule: "+str(roomNames[room])+"</h2>\n"
                x+=generaKindHTML(table,1,room,filter_curriculum, lectureNames,roomNames)
                x+= "\n"
        x+='</div>'
        return x
def getLectureShorted(lecture):
	acron=''
	for word in lecture.split(' '):
		acron=acron+word[0:1]
	return acron
		
'''
	kind = 1 for rooms
	kind = 0 for lecture
	value = the id of the room or lecture selected
'''
def generaKindHTML(tabella,kind, value,filter_curriculum, lectureNames,roomNames):
    x='<table>\n'
    x=x+getTableHeaderHTML()
    for hour in range(0,10):
			x+='<tr>\n'
			x+=  ("<td>"+str(hour+8)+":00</td> ")
			for day in range(0,5):
					find = 0;
					x+="<td>"
					for elem in tabella[day][hour]:
						if(kind==0):
							if(str((filter_curriculum[elem[kind]]))==str(value)):
								lecturename=lectureNames[elem[0]]
								roomname=roomNames[elem[1]]
								x+=  ("<div class=\"tab"+str(elem[0])+"\"><span class=\"room\">"+roomname+'</span><br /><span class="lecture">'+(lecturename)+'</span></div>')
						else:
							if(str((elem[kind]))==str(value)):
								lecturename=lectureNames[elem[0]]
								roomname=roomNames[elem[1]]
								x+=  ("<div class=\"tab"+str(elem[0])+"\"><span class=\"room\">"+roomname+'</span><br /><span class="lecture">'+(lecturename)+'</span></div>')
					x+='</td>'
			x+=  "</tr>\n"
    x+='</table>'
    return x
		

def main(argv):
        inputfile = 'prova'
        outputfile = 'stdout'
        environment= 'deptplanner2.dzn';
        fileformat = 'table'
        try:
                opts, args = getopt.getopt(argv,"hi:e:o:f:",["ifile=","ofile=","efile","format"])
        except getopt.GetoptError:
                print 'createTable.py [-i <inputfile>] [-e <environment>] [-o <outputfile>] [-f <format>]'
                sys.exit(2)
        for opt, arg in opts:
                if opt == '-h':
                        print 'createTable.py [-i <inputfile>] [-e <environment>] [-o <outputfile>] [-f <format>]'
                        sys.exit()
                elif opt in ("-i", "--ifile"):
                        inputfile = arg
                elif opt in ("-e", "--efile"):
                        environment = arg
                elif opt in ("-o", "--ofile"):
                        outputfile = arg
                elif opt in ("-f", "--format"):
                        fileformat = arg

        if(outputfile=="stdout"):		
                x=generateTable(inputfile,environment)
                print(x)
        else:
                if(fileformat=='table'):
                        x=generateTable(inputfile,environment)
                        out_file = open(outputfile,"w")
                        out_file.write(x)
                        out_file.close()
                elif(fileformat=='html'):
                        x=generateTableHTML(inputfile,environment)
                        out_file = open(outputfile,"w")
                        out_file.write(x)
                        out_file.close()		

if __name__ == "__main__":
    main(sys.argv[1:])
