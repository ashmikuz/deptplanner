DATAFORMAT
==========

The python script generatedata.py take all files in the data folder to generate the data.dzn file,
the process is really simple but data format must be strictly respected.

the *data* folder must contain 4 subfolders: *global*, *lectures*, *rooms*, *teachers*

#Global
the *global* folder must contain a file named: global
this file is intended to contain the following environment variables

maxlecturehoursinday=4;

maxteacherhours=6;

maxcurriculumhours=8;

minlecturehours=2;

mindaylectures=1;

this variables influence the whole dataset.

#Lecture
the *lectures* folder must contain a list of file, each one represents a single lecture, each lecture must be described by
this traits.

//Name: is the name of the lecture

Name: Algebra e Geometria

//Curricula: is the array of curricula of this lecture

Curriculum: ["Informatica I Anno"]

//Teachers: is the array of teacher who teach this lecture

Teachers: ["TecherName"]

//this represent the list of num hours requested by each teacher for his part of lecture, [5,4,8,6]

TeacherNumHours: [6]

//AllocReq: represent a list of list, this list contain the list of allocation of lecture that each teacher request, if the first teacher requests in his 5 hours of theaching of splitting his lecture in 3 and 2 it become [[3,2],[2,2],[3,2,3],[3,3]]. the default is a void list of void list.

AllocReq: [[]]

//Number of student that follow this lecture

StudentNo: 100

//The name of the lecture which this lecture must not be schedule in the same time

Conflict: ""

//If this lecture is an optional lecture

Optional: False

//If this lecture require a specifical room in which must be done, must contain the name of the room

RequestedRoom: ""

//if the students of this lecture must be splitted in two class A-M, M-Z due to teaching purpose.

SplitStudents: False

//If this lecture must be followed by another lecture, must contain the name of the lecture

SuccessiveLecture: ""

#Room
the *rooms* folder must contain a list of files each one represent a single room, each room must be described by this traits:

Name: Carinci

Capacity: 150

UnavaliableSlots: []

UnavaliableSlots is a list of timeslot where the lecture must not be scheduled in this room, it's described as:

SD-ED|sh-eh

SD: starting day

ED: ending day (optional)

sh: starting hour

eh: ending hour (optional)


Some example:
* 1-2|17-18 Monday and Tuesday between 17 to 18
* 1|13-14   on Monday between 13 to 14
* 1         on Monday between 8 to 18
* 2|15      on Tuesday between 15 to 18
* 2|-13     on Tuesda between 8 to 13

#Teacher
the *teachers* folder must contain a list of files each one represent a single teacher, each teacher must be described by this traits:

Name: Asperti

RequestedConsecutiveDays: 0

UnavaliableSlots: []

UnavaliableSlot is the same as described in the room section.

RequestConsecutiveDays express the fact that the teacher want to teach in a few number of consecutive days due to his personal needs.






